#include <iostream>
#include <memory>
#include <string>
#include <vector>

using namespace std;

class Component {
public:
    virtual ~Component() = default;
    virtual string name() const = 0;
    virtual int size() const = 0;
    virtual bool is_text_file() const = 0;
};

class File : public Component {
public:
    File(string name, int size, bool is_text_file) : name_(move(name)), size_(size), is_text_file_(is_text_file) {}
    string name() const override { return name_; }
    int size() const override { return size_; }
    bool is_text_file() const override { return is_text_file_; }
private:
    string name_;
    int size_;
    bool is_text_file_;
};

class Folder : public Component {
public:
    Folder(string name) : name_(move(name)) {}

    void add(shared_ptr<Component> component) {
        components_.push_back(move(component));
    }

    string name() const override { return name_; }

    int size() const override {
        int total_size = 0;
        for (const auto& component : components_) {
            total_size += component->size();
        }
        return total_size;
    }

    bool is_text_file() const override {
        bool result = false;
        for (const auto& component : components_) {
            result |= component->is_text_file();
        }
        return result;
    }

private:
    string name_;
    vector<shared_ptr<Component>> components_;
};

int main() {
    auto file1 = make_shared<File>("file1.txt", 100, true);
    auto file2 = make_shared<File>("file2.png", 75, false);
    auto folder1 = make_shared<Folder>("folder1");
    folder1->add(file1);
    folder1->add(file2);
    auto file3 = make_shared<File>("file3.txt", 50, true);
    auto folder2 = make_shared<Folder>("folder2");
    folder2->add(file3);
    folder2->add(folder1);
    cout << "Size of folder2: " << folder2->size() << endl;
    return 0;
}
