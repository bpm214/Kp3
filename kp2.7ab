//a 

#include <cmath>

using namespace std;

class Point {
public:
    virtual ~Point() {}
    virtual float getX() const = 0;
    virtual float getY() const = 0;
};

class CartesianPoint : public Point {
public:
    CartesianPoint(float x, float y) : x_{x}, y_{y} {}
    float getX() const override { return x_; }
    float getY() const override { return y_; }
private:
    float x_, y_;
};

class PolarPoint : public Point {
public:
    PolarPoint(float r, float phi) : r_{r}, phi_{phi} {}
    float getX() const override { return r_ * cos(phi_); }
    float getY() const override { return r_ * sin(phi_); }
private:
    float r_, phi_;
};

class PointFactory {
public:
    virtual ~PointFactory() {}
    virtual Point* createPoint(float x, float y) const = 0;
};

class CartesianPointFactory : public PointFactory {
public:
    Point* createPoint(float x, float y) const override {
        return new CartesianPoint(x, y);
    }
};

class PolarPointFactory : public PointFactory {
public:
    Point* createPoint(float r, float phi) const override {
        return new PolarPoint(r, phi);
    }
};

int main() {
    PointFactory* factory = new PolarPointFactory();
    Point* p = factory->createPoint(5, M_PI_4);
    PointFactory* factory2 = new CartesianPointFactory();
    Point* my_point = factory2->createPoint(3, 4);

    delete p;
    delete factory;
    delete my_point;
    delete factory2;

    return 0;
}


//b

#include <cmath>

using namespace std;

class Point {
public:
    Point(float x, float y) : x_{x}, y_{y} {}

private:
    float x_,y_;
};

class PointFactory {
public:
    static Point NewCartesian(float x, float y) {
        return Point(x, y);
    }

    static Point NewPolar(float r, float phi) {
        float x = r * cos(phi);
        float y = r * sin(phi);
        return Point(x, y);
    }
};

int main() {
    auto p = PointFactory::NewPolar(5, M_PI_4);
    auto my_point = PointFactory::NewCartesian(3, 4);
    return 0;
}
